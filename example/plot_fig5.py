"""
Figure 5
========

Plot measures from figure 5
"""
import matplotlib.pyplot as plt
import pandas as pd

from intrieri1997 import pth_clean

# read meas
meas = pd.read_csv(pth_clean / "fig5.csv", sep=";", comment="#", index_col=['solar_time'])

# plot result
fig, axes = plt.subplots(1, 2, sharex='all', sharey='all', figsize=(10, 5), squeeze=False)
for j, mod in enumerate(['bef', 'aft']):
    df = meas[meas['mod'] == mod]

    ax = axes[0, j]
    ax.set_title(f"{mod}")
    ax.plot(df.index, df['tva'], lw=3, label="TVA")

    ax.set_ylabel("TVA [µmol C02.m-2.s-1]")
    ax.set_ylim(-5, 95)
    ax.set_yticks(range(0, 91, 10))

    ax = ax.twinx()
    ax.plot(df.index, df['co2'], '--', label="CO2")
    ax.plot(df.index, df['par'], label="PAR")

    ax.set_ylabel("co2 [µl.l-1] PAR [µmol photon.m-2.s-1]")
    ax.set_ylim(0, 1900)
    ax.set_yticks(range(0, 1801, 200))

for ax in axes[-1, :]:
    ax.set_xlabel("Solar time [h]")
    ax.set_xlim(6, 20)
    ax.set_xticks(range(6, 21, 2))

fig.tight_layout()
plt.show()
