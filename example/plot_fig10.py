"""
Figure 10
=========

Plot measures from figure 10
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from intrieri1997 import fig10, pth_clean

# read meas
meas_a = pd.read_csv(pth_clean / "fig10a.csv", sep=";", comment="#")
meas_b = pd.read_csv(pth_clean / "fig10b.csv", sep=";", comment="#")

# regression
cls = np.linspace(5, 8.5, 100)
reg_a = [fig10.reg_a(cl) for cl in cls]

tva_yields = np.linspace(2, 7, 100)
reg_b = [fig10.reg_b(ty) for ty in tva_yields]

# plot result
fig, axes = plt.subplots(1, 2, figsize=(10, 4), squeeze=False)
ax = axes[0, 0]
for vid, df in meas_a.groupby('vine'):
    ax.plot(df['crop_load'], df['brix'], 'o', label=f"vine {vid:d}")

ax.plot(cls, reg_a, '--')

ax.legend(loc='lower center', ncol=4)
ax.set_ylabel("soluble solids [°brix]")
ax.set_ylim(14, 21)
ax.set_yticks(range(14, 22, 1))

ax.set_xlabel("leaf/fruit ratio [cm2.g-1]")
ax.set_xlim(5, 9)
ax.set_xticks(range(5, 10, 1))

ax = axes[0, 1]
for vid, df in meas_b.groupby('vine'):
    ax.plot(df['tva_yield'], df['brix'], 'o', label=f"vine {vid:d}")

ax.plot(tva_yields, reg_b, '--')

ax.legend(loc='lower center', ncol=4)
ax.set_ylabel("soluble solids [°brix]")
ax.set_ylim(14, 21)
ax.set_yticks(range(14, 22, 1))

ax.set_xlabel("tva / yield [nmol CO2.s-1.g-1]")
ax.set_xlim(2, 8)
ax.set_xticks(range(2, 9, 1))

fig.tight_layout()
plt.show()
