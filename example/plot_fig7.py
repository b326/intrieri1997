"""
Figure 7
========

Plot measures from figure 7
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from intrieri1997 import fig7, pth_clean

# read meas
meas = pd.read_csv(pth_clean / "fig7.csv", sep=";", comment="#")

# regression
las = np.linspace(9, 13, 100)
reg = fig7.reg_sva(las)

# plot result
fig, axes = plt.subplots(1, 2, sharex='all', figsize=(10, 4), squeeze=False)
ax = axes[0, 0]
for trt, df in meas.groupby('treatment'):
    ax.plot(df['leaf_area'], df['tva'], 'o', label=trt)

ax.legend(loc='lower left')
ax.set_ylabel("tva [µmol CO2.plant-1.s-1]")
ax.set_ylim(15, 40)
ax.set_yticks(range(15, 41, 5))

ax = axes[0, 1]
for trt, df in meas.groupby('treatment'):
    ax.plot(df['leaf_area'], df['sva'], 'o', label=trt)

ax.plot(las, reg, '--')

ax.legend(loc='lower left')
ax.set_ylabel("sva [µmol CO2.m-2.s-1]")
ax.set_ylim(1, 4)
ax.set_yticks(range(1, 5, 1))

for ax in axes[-1, :]:
    ax.set_xlabel("leaf_area [m2]")
    ax.set_xlim(8, 14)
    ax.set_xticks(range(8, 15, 1))

fig.tight_layout()
plt.show()
