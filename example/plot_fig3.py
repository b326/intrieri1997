"""
Figure 3
========

Plot measures from figure 3
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from intrieri1997 import fig3, pth_clean

# read meas
meas = pd.read_csv(pth_clean / "fig3.csv", sep=";", comment="#")

# regressions
ppfds = np.linspace(0, 2000, 100)
reg = dict(
    single_leaf=[fig3.reg_single_leaf(x) for x in ppfds],
    natural_bc=[fig3.reg_natural_bc(x) for x in ppfds],
    restricted_bc=[fig3.reg_restricted_bc(x) for x in ppfds],
)

df_sl = meas.loc[meas['scale'] == 'single_leaf']

# plot result
fig, axes = plt.subplots(1, 1, figsize=(7, 5), squeeze=False)
ax = axes[0, 0]
for scale, df in meas.groupby('scale'):
    crv, = ax.plot(df['ppfd'], df['an'], 's', label=scale)
    ax.plot(ppfds, reg[scale], '-', color=crv.get_color())

ax.legend(loc='upper left')
ax.set_ylabel("A [µmol C02.m-2.s-1]")
ax.set_ylim(-1, 17)
ax.set_yticks(range(0, 17, 2))

ax.set_xlabel("PAR [µmol photon.m-2.s-1]")
ax.set_xlim(0, 2000)
ax.set_xticks(range(0, 2001, 250))

fig.tight_layout()
plt.show()
