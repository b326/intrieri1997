"""
Figure 6
========

Plot measures from figure 6
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from intrieri1997 import fig6, pth_clean

# read meas
expe = pd.read_csv(pth_clean / "table1.csv", sep=";", comment="#", index_col=['treatment'])
expe_end = pd.DataFrame([dict(treatment='end', date='1995-09-30')]).set_index(['treatment'])

expe = pd.concat([expe, expe_end]).fillna(method='ffill')
expe['doy'] = expe['date'].apply(lambda date: pd.Timestamp(date).dayofyear)

meas = pd.read_csv(pth_clean / "fig6.csv", sep=";", comment="#", index_col=['doy'])
doy_first = 227
doy_last = 273

day_first = pd.Timestamp(1995, 1, 1) + pd.Timedelta(days=doy_first - 1)
day_last = pd.Timestamp(1995, 1, 1) + pd.Timedelta(days=doy_last - 1)

# regressions
doys = np.arange(doy_first, doy_last + 1)
reg = fig6.reg_sva(doys - pd.Timestamp(1995, 8, 1).dayofyear)

# plot result
fig, axes = plt.subplots(1, 1, figsize=(7, 5), squeeze=False)
ax = axes[0, 0]
crv, = ax.plot(meas.index, meas['sva'], 'o', label="SVA")
ax.plot(doys, reg, '--', color=crv.get_color())
ax.plot(meas.index, meas['tva'], 'o', label="TVA")

ax.step(expe['doy'], 100 - expe['la_removed_frac'], where='post')
for (_, row_bef), (_, row_aft) in zip(expe.iloc[:-1].iterrows(), expe.iloc[1:].iterrows()):
    ax.text((row_bef['doy'] + row_aft['doy']) / 2, 95 - row_bef['la_removed_frac'], f"{row_bef['leaf_area']:.1f}",
            ha='center', va='top')

ax.legend(loc='center left')
ax.set_ylabel("[%]")
ax.set_ylim(0, 150)
ax.set_yticks(range(0, 150, 20))

ax.annotate(f"{day_first.date()}", (doy_first, 0), (10, 30), textcoords='offset pixels',
            ha='left', va='center', arrowprops=dict(arrowstyle="->"))
ax.annotate(f"{day_last.date()}", (doy_last, 0), (-10, 30), textcoords='offset pixels',
            ha='right', va='center', arrowprops=dict(arrowstyle="->"))
ax.set_xlabel("doy")
ax.set_xlim(doy_first, doy_last)
ax.set_xticks(range(doy_first, doy_last + 1, 5))

fig.tight_layout()
plt.show()
