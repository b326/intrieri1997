"""
Figure 8
========

Plot measures from figure 8
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from intrieri1997 import fig8, pth_clean

# read meas
expe = pd.read_csv(pth_clean / "table2.csv", sep=";", comment="#")
expe = expe.groupby('treatment').agg({'date': 'first', 'leaf_area': 'mean', 'la_removed_frac': 'mean'})
expe_end = pd.DataFrame([dict(treatment='end', date='1996-08-23')]).set_index(['treatment'])

expe = pd.concat([expe, expe_end])
expe['day'] = expe['date'].apply(lambda date: pd.Timestamp(date).day)

expe = expe.sort_values(by='day').fillna(method='ffill')

meas = pd.read_csv(pth_clean / "fig8.csv", sep=";", comment="#", index_col=['day'])

# regressions
days = np.linspace(1, 22, 100)
reg = [fig8.reg_tva(day) for day in days]

# plot result
fig, axes = plt.subplots(1, 1, figsize=(7, 5), squeeze=False)
ax = axes[0, 0]
ax.plot(meas.index, meas['sva'], 'o', label="SVA")
crv, = ax.plot(meas.index, meas['tva'], 'o', label="TVA")
ax.plot(days, reg, '--', color=crv.get_color())

ax.step(expe['day'], 100 - expe['la_removed_frac'], where='post')
for (_, row_bef), (_, row_aft) in zip(expe.iloc[:-1].iterrows(), expe.iloc[1:].iterrows()):
    ax.text((row_bef['day'] + row_aft['day']) / 2, 105 - row_bef['la_removed_frac'], f"{row_bef['leaf_area']:.1f}",
            ha='center', va='bottom')

ax.legend(loc='lower left')
ax.set_ylabel("[%]")
ax.set_ylim(0, 150)
ax.set_yticks(range(0, 150, 20))

ax.set_xlabel("august 1996")
ax.set_xlim(0, 31)
ax.set_xticks([1, 5, 10, 15, 20, 25, 31])

fig.tight_layout()
plt.show()
