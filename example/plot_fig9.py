"""
Figure 9
========

Plot measures from figure 9
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from intrieri1997 import fig9, pth_clean

# read meas
meas = pd.read_csv(pth_clean / "fig9.csv", sep=";", comment="#")

# regression
las = np.linspace(4, 11, 100)
reg = fig9.reg_tva(las)

# plot result
fig, axes = plt.subplots(1, 2, sharex='all', figsize=(10, 4), squeeze=False)
ax = axes[0, 0]
for vid, df in meas.groupby('vine'):
    ax.plot(df['leaf_area'], df['tva'], 'o', label=f"vine {vid:d}")

ax.plot(las, reg, '--')

ax.legend(loc='lower center', ncol=4)
ax.set_ylabel("tva [µmol CO2.plant-1.s-1]")
ax.set_ylim(0, 75)
ax.set_yticks(range(0, 75, 10))

ax = axes[0, 1]
for vid, df in meas.groupby('vine'):
    ax.plot(df['leaf_area'], df['sva'], 'o', label=f"vine {vid:d}")

ax.legend(loc='lower center', ncol=4)
ax.set_ylabel("sva [µmol CO2.m-2.s-1]")
ax.set_ylim(0, 8.5)
ax.set_yticks(range(0, 9, 1))

for ax in axes[-1, :]:
    ax.set_xlabel("leaf_area [m2]")
    ax.set_xlim(4, 12)
    ax.set_xticks(range(4, 13, 1))

fig.tight_layout()
plt.show()
