"""
Regression from fig7 legend
"""


def reg_sva(x):
    return 3.67 - 0.000163 * x ** 3.49
