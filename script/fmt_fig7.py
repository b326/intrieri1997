"""
Format measurements from fig7
"""
from pathlib import Path

import pandas as pd
from graphextract.svg_extractor import extract_data

from intrieri1997 import pth_clean

expe = pd.read_csv(pth_clean / "table1.csv", sep=";", comment="#", index_col=['treatment'])

dfs = []
for suffix in ('tva', 'sva'):
    meas = extract_data(f"../raw/fig7_{suffix}.svg", x_formatter=int, y_formatter=int)

    records = []
    for trt, pts in meas.items():
        leaf_area_meas = sum([la for la, _ in pts]) / len(pts)
        leaf_area = expe.loc[trt, 'leaf_area']
        assert abs(leaf_area - leaf_area_meas) < 0.1

        for _, val in pts:
            records.append({
                'treatment': trt,
                'leaf_area': leaf_area,
                suffix: val
            })

    dfs.append(pd.DataFrame(records).set_index(['treatment', 'leaf_area']))

df = pd.concat(dfs, axis='columns')

# write resulting dataframe in a csv file
df = df[sorted(df.columns)]

with open(pth_clean / "fig7.csv", 'w', encoding='utf-8') as fhw:
    fhw.write(f"# This file has been generated by {Path(__file__).name}\n#\n")
    fhw.write("#\n")
    fhw.write("# treatment: description of leaf removal\n")
    fhw.write("# leaf_area: [m2] leaf area after defoliation\n")
    fhw.write("# sva: [µmol CO2.m-2.s-1] specific vine assimilation\n")
    fhw.write("# tva: [µmol CO2.plant-1.s-1] total vine assimilation\n")
    fhw.write("#\n")
    df.to_csv(fhw, sep=";", lineterminator="\n", float_format="%.2f")
